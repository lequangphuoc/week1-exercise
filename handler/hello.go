package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type Data struct {
	Name    string
	Age     int
	Address string
}

// Handler
func Hello(c echo.Context) (err error) {
	type User struct {
		Name  string `json:"name" form:"name" query:"name"`
		Email string `json:"email" form:"email" query:"email"`
	}

	u := new(User)
	if err = c.Bind(u); err != nil {
		return
	}

	return c.JSON(http.StatusOK, u)
}
