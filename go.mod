module gitlab.com/nordic-coder/training/week1-exercise

go 1.12

require (
	github.com/labstack/echo/v4 v4.0.0
	gitlab.com/nordic-coder/helper v0.0.1
)
